# News Reader

**URL's**

Front-end is at: `/`  
API endpoint is at: `/api/news/news-by-channel`  
Other API endpoints: `/api/news-by-channel/{id}` and `/api/channel-options`   

**Installation Commands:**

`composer install`  
`npm install`

**Deployment Commands:**

`php artisan migrate`  
`php artisan news:collect`  
`php artisan db:seed --class=SeedChannels`

**Front-end Build Commands:**

`npm run dev` (*this must be run at least once)  
`npm run watch`
