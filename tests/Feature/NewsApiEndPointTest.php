<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use Illuminate\Support\Facades\Route;

class NewsApiEndPointTest extends TestCase
{

    /**
     * A test to test the news end-point.
     *
     * @return void
     */

    public function testNewsRouteExists()
    {
        $this->assertTrue(Route::has('news'));
    }

    public function testNewsIsApiResponse()
    {
        // Not worked out the best way to do this just yet.
        $this->assertTrue(true);
    }

    public function testNewsIsJson()
    {

        $response = $this->get(route('news'));
        $data = $response->json();

        $this->assertNotEmpty($data);

    }

    public function testNewsHasData()
    {

        $response = $this->get(route('news'));
        $collect = Collect($response->json());

        $this->assertTrue($collect->has("data"));

    }

    public function testNewsDataIsNotEmpty()
    {

        $response = $this->get(route('news'));
        $collect = Collect($response->json());

        $this->assertNotEmpty($collect->get("data"));

    }

}
