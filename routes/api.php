<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* NOTE Start */
/*
 * This is pointing to a resource controller, however its a custom function.
 * It will only work if the route is above the resource routs.
 *
 * So is has to be above, Route::resource('/news', 'NewsReaderController');
 *
*/
Route::get('/news-by-channel/{id?}', 'NewsReaderController@newsArticleByChannel')->name('news_by_channel');
/* NOTE END */

Route::resource('/news', 'NewsReaderController', [
    'names' => [
        'show' => 'news.article',
    ]
]);

Route::resource('/channel-options', 'ChannelsController');
