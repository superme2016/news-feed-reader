<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

// Service
use App\Services\Commands\NewsCollector;

class CollectNewsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'news:collect';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Collects news from multiple sources.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->info('News Collection Started!');

        if(!(new NewsCollector)->collectNews())
        {
            $this->info('News collection finished successfully. ');
            return;
        }

        $this->info('Issue with collecting news, please check logs for details.');

    }
}
