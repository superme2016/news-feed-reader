<?php

namespace App\Services\NewsCollection;

use Illuminate\Support\Facades\Log;

class NewsFeedChecks {

    public function checkNewsFeed($url)
    {

        $newsFeed      = null;
        $issueDetected = false;

        if(($content = @file_get_contents($url)) === false)
        {
            Log::error("Nothing found at: {$url}");
            $issueDetected = true;
        }

        if(($newsFeed = simplexml_load_string($content)) === false)
        {
            Log::error("Bad XML read form: {$url}");
            $issueDetected = true;
        }

        if(!$newsFeed)
        {
            Log::error("No newsfeed found in news feed: {$url}");
            $issueDetected = true;
        }
        else
        {
            if(!$newsFeed->channel)
            {
                Log::error("No channel found in news feed: {$url}");
                $issueDetected = true;
            }

            if(!count($newsFeed->channel->item))
            {
                Log::error("No items found in channel: {$url}");
                $issueDetected = true;
            }
        }

        return [
            'data'           => $newsFeed,
            'issue_detected' => $issueDetected
        ];

    }

}