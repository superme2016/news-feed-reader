<?php

namespace App\Services\NewsCollection;

use Illuminate\Support\Facades\Log;

// Services
use App\Services\NewsArticle\CreateNewsArticle;
use App\Services\NewsChannel\CreateNewsChannel;

class ProcessNewsFeed {

    public function processNewsFeed($channel)
    {

        $newsChannel['title']        = (string)$channel->title;
        $newsChannel['link']         = (string)$channel->link;
        $newsChannel['description']  = (string)$channel->description;
        $newsChannel['image']        = (string)$channel->image->url;

        // Create a news source via the service based on the RSS news feed.
        $channelID = (new CreateNewsChannel)->storeNewsChannel($newsChannel);

        foreach ($channel->item as $item)
        {

            $newsArticle['title']          = (string)$item->title;
            $newsArticle['link']           = (string)$item->link;
            $newsArticle['comments_link']  = (string)$item->comments;
            $newsArticle['pub_date']       = (string)$item->pubDate;
            $newsArticle['guid']           = (string)$item->guid;
            $newsArticle['description']    = (string)$item->description;

            // Makes the encoded attribute available "(string)$item->encoded".
            // *!! Note !!*, if done above it will null out all the other attributes.
            $item = $item->children('content', true);
            $newsArticle['content']        = (string)$item->encoded;

            // Sets the channel_id that will be used to hookup the relation between the channel and the article.
            if($channelID)
            {
                $newsArticle['news_channel_id'] = $channelID;
            }

            // Create the news article via the service.
            (new CreateNewsArticle)->storeNewsArticle($newsArticle);

        }

    }

}