<?php

namespace App\Services\NewsArticle;

// Models
use App\Models\NewsArticle;

use Illuminate\Support\Facades\Log;

class CreateNewsArticle
{

    public function storeNewsArticle(Array $newsArticle)
    {

        //Quick conversion over to a collection to avoid using !isset and not null.
        $newsArticle = collect($newsArticle);

        /*
         * Some pre-checks on the input data.
         *
         */
        if(!$newsArticle->has('guid'))
        {
            if (\App::environment('local'))
            {
                Log::info("No News article guid provided. {$newsArticle->get('guid')}");
            }

            return;
        }

        if(NewsArticle::where("guid", $newsArticle->get('guid'))->value('guid'))
        {
            if (\App::environment('local'))
            {
                Log::info("News article already exists. {$newsArticle->get('guid')}");
            }

            return;
        }

        /*
         * Storing the new news article to the DB.
         * Used a mass assign fill and a save in one chained model instance.
         *
         * Also swapping the collection back over to an array
         * as the mass assign requires an array.
         *
         */

        if(!(new NewsArticle)->fill($newsArticle->toArray())->save())
        {
            Log::info("Unable to save News article");
        }
    }

}