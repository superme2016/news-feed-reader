<?php

namespace App\Services\NewsChannel;

// Models
use App\Models\NewsChannel;

use Illuminate\Support\Facades\Log;

class CreateNewsChannel
{

    public function storeNewsChannel(Array $newsChannel)
    {

        // Quick conversion over to a collection to avoid using !isset and not null.
        $newsChannel = collect($newsChannel);

        /*
         * Some pre-checks on the input data.
         *
         */
        if(!$newsChannel->has('link'))
        {
            if (\App::environment('local')) {
                Log::info("No News source link provided. {$newsChannel->get('link')}");
            }

            return;
        }

        if(NewsChannel::where("link", $newsChannel->get('link'))->value('link'))
        {

            if (\App::environment('local'))
            {
                Log::info("News source already exists for {$newsChannel->get('link')}");
            }

            return;
        }

        /*
         * Storing the new news channel to the DB.
         *
         * Also swapping the collection back over to an array
         * as the mass assign requires an array.
         *
         */

        $newsChannel = (new NewsChannel)->fill($newsChannel->toArray());

        if(!$newsChannel->save())
        {
            Log::info("Unable to save News Source");
        }

        return $newsChannel->id;

    }

}