<?php

namespace App\Services\Commands;

// Services
use App\Services\NewsCollection\NewsFeedChecks;
use App\Services\NewsCollection\ProcessNewsFeed;

use Illuminate\Support\Facades\Log;

class NewsCollector
{

    protected $urls;
    protected $issueDetected = false;

    public function __construct($urls = null)
    {
        if($urls)
        {
            $this->urls = $urls;
        }
        else
        {
            $this->urls = [
                "https://metro.co.uk/feed/",
                "http://feeds.skynews.com/feeds/rss/uk.xml",
                "http://feeds.skynews.com/feeds/rss/business.xml",
                "http://feeds.24.com/articles/sport/featured/topstories/rss"
            ];
        }

    }

    public function collectNews()
    {
        foreach ($this->urls as $url)
        {

            // Run checks on the feed before processing.
            $newsFeed = (new NewsFeedChecks())->checkNewsFeed($url);

            // Must be a better way to handle this, maybe inheritance.
            if($newsFeed['issue_detected'])
            {
                $this->issueDetected = $newsFeed['issue_detected'];
                continue;
            }

            foreach ($newsFeed['data']->channel as $channel)
            {
                (new ProcessNewsFeed())->processNewsFeed($channel);
            }

        }

        return $this->issueDetected;
    }

    // TODO:: Left this in to be seen, but will be removed as this function is no longer being used.
    private function requestData($url)
    {

        // TODO Pass in the content type also.

        /*
         * Did want to use Guzzle parallel batching, but could not get it to work
         * https://guzzle3.readthedocs.io/batching/batching.html#batching
         *
         */

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return ["error" => $err];
        } else {
            return $response;
        }

    }

}
