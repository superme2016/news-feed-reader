<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\NewsChannel;

class NewsArticle extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'link',
        'comments_link',
        'pub_date',
        'guid',
        'description',
        'content',
        'news_channel_id'
    ];

    public function channel()
    {
       return $this->hasOne(NewsChannel::class, 'id', 'news_channel_id');
    }
}
