<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{

    /*
     * Probably not needed.
     */
    public function newsArticles(){
        return $this->hasMany(NewsArticle::class, 'news_channel_id', 'news_channel_id')->orderBy('id', 'desc');
    }

}
