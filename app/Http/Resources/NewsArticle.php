<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewsArticle extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        $data = [
            'title'       => $this->title,
            'link'        => $this->link,
            'pub_date'    => str_replace(['+0000', '+0200'], '', $this->pub_date),
            'guid'        => $this->guid,
            'description' => $this->description,
            'content'     => $this->content, //TODO:: Move this over the a relation.
        ];

        if($this->channel)
        {
            $channel = [
                'title'       => $this->channel->title,
                'link'        => $this->channel->link,
                'image'       => $this->channel->image,
                'description' => $this->channel->description
            ];

            $data['channel'] = $channel;

        }
        else
        {
            $data['channel'] = '';
        }

        return $data;

    }
}
