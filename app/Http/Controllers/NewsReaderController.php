<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\Models\NewsArticle;

// Resource
use App\Http\Resources\NewsArticle as NewsArticleResource;
use App\Http\Resources\NewsArticleCollection;

class NewsReaderController extends Controller
{

    /**
     * Get news by channel id.
     *
     * @return \Illuminate\Http\Response
     */
    public function newsArticleByChannel($id = null)
    {
        if($id)
        {
            $newsArticles = NewsArticle::where('news_channel_id', $id)->orderBy('id', 'DESC')->paginate(5);
        }
        else
        {
            $newsArticles = NewsArticle::orderBy("id", "DESC")->paginate(5);
        }

        return (new NewsArticleCollection($newsArticles));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $newsArticles = NewsArticle::find($id);
        return new NewsArticleResource($newsArticles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
