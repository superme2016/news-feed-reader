<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

// Models
use App\Models\NewsChannel;

class SeedChannels extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Options to match the news_channels.
        $newsChannels = NewsChannel::all();

        foreach ($newsChannels as $newsChannel)
        {
            DB::table('channels')->insert(
                [
                    'name' => $newsChannel->link,
                    'news_channel_id' => $newsChannel->id
                ]
            );
        }

    }
}
