<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_articles', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->string('title', 250);
            $table->string('link', 250);
            $table->string('comments_link', 250);
            $table->string('pub_date', 250);
            $table->string('guid', 250);
            $table->string('description', 500);
            $table->mediumText('content');
            $table->integer('news_channel_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_articles');
    }
}
