<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_channels', function (Blueprint $table) {

            $table->increments('id');
            $table->timestamps();

            $table->string("title", 250);
            $table->string("link", 250);
            $table->string("image", 250);
            $table->string("description", 500);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_channels');
    }
}
